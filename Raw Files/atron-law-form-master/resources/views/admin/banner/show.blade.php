<table class="table table-hover">
  <tr>
      <th>Image</th>
      <td>
          <img src="{{ asset('public/uploads/banner/'.$model->image) }}" alt="{{ $model->banner_title }}" width="300px">
      </td>
  </tr>
    <tr>
        <th>Banner Title</th>
        <td>{{ $model->banner_title }}</td>
    </tr>
    <tr>
        <th>Banner Sub Title</th>
        <td>{{ $model->banner_subtitle }}</td>
    </tr>

    <tr>
        <th>Banner Details</th>
        <td>{!!  $model->details !!}</td>
    </tr>

    <tr>
        <th>Banner Status</th>
        <td>
            @if($model->status == 'active')
                 <span class=" text-success">Active</span>
            @else
                <span class=" text-danger">In Active</span>
            @endif
        </td>
    </tr>
</table>
