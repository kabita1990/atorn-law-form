<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Front\IndexPageController::class, 'index'])->name('index');


Route::prefix('/admin')->group(function (){
    // Admin Login
    Route::get('/login', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminLogin'])->name('adminLogin');
    Route::post('/login', [\App\Http\Controllers\Admin\AdminLoginController::class, 'loginAdmin'])->name('loginAdmin');

    Route::group(['middleware' => 'admin'], function(){
        // Admin Dashboard
        Route::get('/dashboard', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminDashboard'])->name('adminDashboard');
        // Admin Profile
        Route::get('/profile', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfile'])->name('adminProfile');
        // Admin Profile Update
        Route::post('/profile/update/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfileUpdate'])->name('adminProfileUpdate');
        // Delete Image
        Route::get('/delete-image/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'deleteImage'])->name('deleteImage');
        // Change Password
        Route::get('/change-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'changePassword'])->name('changePassword');
        // Check Current Password
        Route::post('/check-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'chkUserPassword'])->name('chkUserPassword');


        Route::get('/theme', [\App\Http\Controllers\Admin\ThemeController::class, 'theme'])->name('theme');
        Route::post('/theme/{id}', [\App\Http\Controllers\Admin\ThemeController::class, 'themeUpdate'])->name('themeUpdate');

        // Banners Routes
        Route::get('/banners', [\App\Http\Controllers\Admin\BannerController::class, 'index'])->name('banner.index');
        Route::get('/banner/add', [\App\Http\Controllers\Admin\BannerController::class, 'add'])->name('banner.add');
        Route::post('/banner/store', [\App\Http\Controllers\Admin\BannerController::class, 'store'])->name('banner.store');
        Route::get('/table/banner', [\App\Http\Controllers\Admin\BannerController::class, 'dataTable'])->name('table.banner');
        Route::get('/changeStatus', [\App\Http\Controllers\Admin\BannerController::class, 'changeStatus'])->name('changeStatus');
        Route::get('/banner/show/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'show'])->name('banner.show');
        Route::get('/banner/edit/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'edit'])->name('banner.edit');
        Route::post('/banner/update/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'update'])->name('banner.update');
        Route::get('/delete-banner/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'delete'])->name('banner.delete');
    });

    Route::get('logout', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminLogout'])->name('adminLogout');
});

