<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;

class IndexPageController extends Controller
{
    // Index Page
    public function index(){
        $banners = Banner::where('status', 'active')->orderBy('priority', 'ASC')->get();
        return view ('front.index', compact('banners'));
    }
}
