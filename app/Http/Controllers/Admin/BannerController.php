<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;





class BannerController extends Controller
{
    //Index Page
    public function index(){
        return view('admin.banner.index');
    }

    //Add Banner
    public function add(){
        return view('admin.banner.add');

    }

    //Store Banner
    public function store(Request $request){
       $data = $request->all();
       $rules = [
        'banner_title' => 'required|max:255',
        'banner_subtitle' => 'required|max:255',
        'details' => 'required',
        'image' => 'required',
        
    ];
    $customMessages = [
        'banner_title.required' => 'Banner title is required',
        'banner_title.max' => 'Banner title is must not be more then 255',

        'banner_subtitle.required' => 'Banner subtitle is required',
        'banner_subtitle.max' => 'You are not allowed to enter more than 255 Characters',
        'details.required' => 'Banner detail is required',

    ];
    $this->validate($request, $rules, $customMessages);
    $banner = new  Banner();
    $banner->banner_title = $data['banner_title'];
    $banner->banner_subtitle = $data['banner_subtitle'];
    $banner->details = $data['details'];
    $banner->priority = $data['priority'];
    $banner->link_1_title = $data['link_1_title'];
    $banner->link_2_title = $data['link_2_title'];
    $banner->link_1_URL = $data['link_1_URL'];
    $banner->link_2_URL = $data['link_2_URL'];

    if(!empty($data['status'])){
        $banner->status = 'active';
    } else {
    $banner->status = 'inactive';
}

$random = Str::slug($data['banner_title']);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/banner/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $banner->image = $filename;
            }
        }

        $banner->save();
        $notification = array(
          'alert-type' => 'success',
          'message' => 'Banner has been Added Successfully'
        );
        return redirect()->back()->with($notification);


    }

    //edit Banner
public function edit($id){
    $banner= Banner::findOrfail($id);
    return view('admin.banner.edit', compact('banner'));
}


 //Update Banner
 public function update(Request $request, $id){
    $data = $request->all();
    $rules = [
        'banner_title' => 'required|max:255',
        'banner_subtitle' => 'required|max:255',
        'details' => 'required',
    ];
    $customMessages = [
        'banner_title.required' => 'Banner Title is required',
        'banner_title.max' => 'Banner Title must not be more than 255 characters',
        'banner_subtitle.required' => 'Banner SubTitle is required',
        'banner_subtitle.max' => 'You are not allowed to enter more than 255 Characters for subtitle',
        'details.required' => 'Banner Details is required',

    ];
    $this->validate($request, $rules, $customMessages);
    $banner = Banner::findOrFail($id);
    $banner->banner_title = $data['banner_title'];
    $banner->banner_subtitle = $data['banner_subtitle'];
    $banner->details = $data['details'];
    $banner->priority = $data['priority'];

    $banner->link_1_title = $data['link_1_title'];
    $banner->link_2_title = $data['link_2_title'];
    $banner->link_1_url = $data['link_1_url'];
    $banner->link_2_url = $data['link_2_url'];

    if(!empty($data['status'])){
        $banner->status = 'active';
    } else {
        $banner->status = 'inactive';
    }

    $random = Str::slug($data['banner_title']);
    $current_date = Carbon::now()->toDateString();
    if($request->hasFile('image')){
        $image_tmp = $request->file('image');
        if($image_tmp->isValid()){
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = $random .'-'. $current_date .'.'. $extension;
            $image_path = 'public/uploads/banner/' . $filename;
            Image::make($image_tmp)->save($image_path);
            $banner->image = $filename;
        }
    }

    $banner->save();

    $image_path = 'public/uploads/banner/';
    if(!empty($data['image'])){
        if(file_exists($image_path.$banner->image)){
            unlink($image_path.$data['current_image']);
        }
    }



    $notification = array(
        'alert-type' => 'info',
        'message' => 'Banner has been Updated Successfully'
    );
    return redirect()->back()->with($notification);

}

    public function dataTable(){
        $model = Banner::orderBy('priority', 'ASC')->get();
        return DataTables::of($model)
        ->addColumn('action', function($model){
            return view('admin.banner._actions',[
               'model' => $model,
               'url_show' => route('banner.show', $model->id),
               'url_edit' => route('banner.edit', $model->id),
               'url_delete' => route('banner.delete', $model->id)



            ]);
        })
        ->editcolumn('status', function($model){
            return view('admin.banner._status',[
                'model'=>$model
            ]);
        })
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->make(true);
        

}







public function show($id){
    $model= Banner::findOrfail($id);
    return view('admin.banner.show', compact('model'));
}



public function changeStatus(Request $request){
   $banners = Banner::findOrfail($request->banner_id);
   $banners->status = $request->status;
   $banners->save();

}

public function delete($id){
    $banner= Banner::findOrfail($id);
    $banner->delete();
    $image_path = 'public/uploads/banner/';
    if(file_exists($image_path.$banner->image)){
        unlink($image_path.$banner->image);
    }
    $notification = array(
        'alert-type' => 'error',
        'message' => 'Banner has been delete Successfully'
      );
      return redirect()->back()->with($notification); 


}
}
