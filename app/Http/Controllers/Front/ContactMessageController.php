<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactMessage;

class ContactMessageController extends Controller
{
    //store message

    public function contactMessage(Request $request){
        $data= $request->all();
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required',
            'subject' => 'required',
            'contact_message'=>'required'

            
        ];
        $customMessages = [
            'name' => 'name is required',
            'email' => 'email is required',
            'phone' => 'Phone Number is required',
            'subject' => 'subject is required',
            'contact_message' => 'Contact Message is required',
    
        ];
        $this->validate($request, $rules, $customMessages);
        $c_message = new ContactMessage();
        $c_message->name =$data['name'];
        $c_message->email = $data['email'];
        $c_message->phone = $data['phone'];
        $c_message->subject = $data['subject'];
        $c_message->contact_message = $data['contact_message'];
        $c_message->save();

        $notification = array(
            'alert-type' => 'info',
            'message' => 'contact has been submitted we will soon get back to you',
          );
          return redirect()->back()->with($notification);
  
    }

}
