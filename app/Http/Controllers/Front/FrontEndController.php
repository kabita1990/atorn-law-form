<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Theme;
use App\Models\Social;


class FrontEndController extends Controller
{
    // Contact Us
    public function contactUs(){
        $site_name = 'Contact Us';
        $page = Page::where('slug', 'contact-us')->first();
        $themeDetail = Theme::first();
        $socialDetail = Social::first();
        return view ('front.pages.contact', compact('site_name','page','themeDetail', 'socialDetail'));
    }
}
