<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Theme;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      View::composer(['admin.*'], function($view){
      $view->with('themes', Theme::first());
      });
    
    
    
      View::composer(['front.*'], function($view){
      $view->with('themes', Theme::first());
      });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
