<html>
<head>
    <title>Forget Password</title>
</head>
<body>
   <table>
       <tr>
           <td> Dear {{ $name }}</td>
       </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
           <td>Your Password Has Been Changed. Your Password information is listed below.</td>
       </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
           <td>New Password: {{ $password }}</td>
       </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
           <td>Thanks and Regards</td>
       </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
           <td>Atorn Law Firm</td>
       </tr>
   </table>
</body>
</html>
