@extends('admin.includes.admin_design')


@section('site_title')
    View All Pages - {{ $themes->website_name }}
@endsection
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Pages</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Pages</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <div class="btn-group">
                    <a href="{{ route('page.add') }}" class="btn btn-primary">Add New page</a>

                </div>
            </div>
        </div>
        <!--end breadcrumb-->
        <hr/>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="banner_datatable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Page Name</th>
                            <th>Page Slug</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>

    </main>
    <!--end page main-->



@endsection

@section('js')




    <script>
        $("#banner_datatable").DataTable({
            processing: true,
            serverSide: true,
            sorting: true,
            searchable: true,
            responsive: true,
            ajax: "{{ route('table.page') }}",
            columns:[
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data:'page_name', name: 'page_name'},
                {data:'slug', name: 'slug'},
                {data:'action', name: 'action', orderable: false},
            ]
        });
    </script>






    <script>
        $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>
@endsection
