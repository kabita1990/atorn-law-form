<!--start sidebar -->
<aside class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{ asset('public/uploads/'.$themes->favicon) }}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">{{ $themes->website_name }}</h4>
        </div>
        <div class="toggle-icon ms-auto"> <i class="bi bi-list"></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="{{route('adminDashboard')}}" >
                <div class="parent-icon"><i class="bi bi-house-fill"></i>
                </div>
                <div class="menu-title">Dashboard</div>
            </a>
            
        </li>


        <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="lni lni-image"></i>
                </div>
                <div class="menu-title">Banner Management</div>
              </a>
              <ul>
                <li> <a href="{{route('banner.add')}}"><i class="bi bi-circle"></i>Add New Banner</a>
                </li>
                <li> <a href="{{route('banner.index')}}"><i class="bi bi-circle"></i>View All Banners</a>
                </li>
            
                </ul>
</li>

<li>
            <a href="{{ route('page.index') }}" >
                <div class="parent-icon"><i class="lni lni-files"></i>
                </div>
                <div class="menu-title">Page Management</div>
            </a>
        </li>


        <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="lni lni-cog"></i>
                </div>
                <div class="menu-title">Settings</div>
              </a>
              <ul>
                <li> <a href="{{route('theme')}}"><i class="bi bi-circle"></i>Theme Settings</a>
                </li>
                <li> <a href="{{route('social')}}"><i class="bi bi-circle"></i>social Settings</a>
                </li>
            
                </ul>
    </ul>
    <!--end navigation-->
</aside>
<!--end sidebar -->
