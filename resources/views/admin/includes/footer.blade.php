
<!-- Bootstrap bundle JS -->
<script src="{{ asset('public/adminpanel/assets/js/bootstrap.bundle.min.js' ) }} "></script>
<!--plugins-->
<script src="{{ asset('public/adminpanel/assets/js/jquery.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/simplebar/js/simplebar.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/metismenu/js/metisMenu.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/js/pace.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/chartjs/js/Chart.min.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/chartjs/js/Chart.extension.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/plugins/apexcharts-bundle/js/apexcharts.min.js' ) }} "></script>
<!--app-->

<script src="{{asset('public/adminpanel/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/adminpanel/assets/plugins/datatable/js/dataTables.bootstrap5.min.js')}}"></script>
  <script src="{{asset('public/adminpanel/assets/js/table-datatable.js')}}"></script>

<script src="{{ asset('public/adminpanel/assets/js/jquery.sweet-alert.custom.js') }}"></script>
<script src="{{ asset('public/adminpanel/assets/js/sweetalert.min.js') }}"></script>

<script src="{{ asset('public/adminpanel/assets/js/app.js' ) }} "></script>
<script src="{{ asset('public/adminpanel/assets/js/index.js' ) }} "></script>
<script>
    new PerfectScrollbar(".best-product")

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>

<script>




<script>
  $(function() {
    $('.status').bootstrapToggle({
      on: 'Mark As Active',
      off: 'Mark As Inactive'
    });
  })

  $(function() {
    $('.status_banner').bootstrapToggle({
      on: 'Active',
      off: 'Inactive'
    });
  })

</script>
<script>
    @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch (type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
</script>

@yield('js')


</body>

</html>
