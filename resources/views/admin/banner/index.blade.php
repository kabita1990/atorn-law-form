@extends('admin.includes.admin_design')

@section('site_title')
 View All Banners- {{ $themes->website_name}}
 @endsection

@section('content')
<!--start content-->
<main class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Banners</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Banners</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('banner.add')}}" class="btn btn-primary">Add New Banner</a>
							
							
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<hr/>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="banner_datatable" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>S.N</th>
										<th>Image</th>
										<th>Banner Title</th>
										<th>Banner sub Title</th>
										<th>Status</th>
										<th>Actions </th>
									</tr>
								</thead>
								
								
							</table>
						</div>
					</div>
				</div>
			
								
			</main>
       <!--end page main-->


	   <!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="model-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
      </div>

    </div>
  </div>
</div>


@endsection



@section('js')



<script>
    $("#banner_datatable").DataTable({

        processing:true,
        serverSide:true,
        sorting:true,
        searchable:true,
        responsive:true,
        ajax:"{{route('table.banner')}}",
        columns:[
            {data: 'DT_RowIndex', name:'DT_RowIndex'},
            {data: 'image', name:'image',
                render: function(data, type, full, meta){
                    if(data){
                        return "<img src={{ URL::to('/') }}/public/uploads/banner/" + data + " width='100'>";
                    }
                }
            },
            {data:'banner_title', name: 'banner_title'},
            {data:'banner_subtitle', name: 'banner_subtitle'},
            {data:'status', name: 'status'},
            {data:'action', name:'action', oderable:false},
        ]
    });
</script>
<script>
        $('body').on('click', '.status_banner', function (event) {
           var status = $(this).prop('checked') == true ? 'active' : 'inactive';
           var banner_id = $(this).data('id');
           $.ajax({
              type: 'GET',
              dataType: "json",
              url: '{{ route('changeStatus') }}',
               data: {'status' : status, 'banner_id': banner_id},
               success: function (data) {
                  console.log('Success');
               }
           });
        });

</script>

<script>
        $('body').on('click', '.btn-show', function (event) {
            event.preventDefault();

			var mytitle = $(this).data('title');
            var me = $(this),
                url = me.attr('href');
                

             $('#model-title').text(mytitle);
             $.ajax({
                url: url,
                dataType: 'html',
                success: function(response){
                    $('#modal-body').html(response);
                }
             });
            $('#modal').modal('show');
        });
    </script>

<script>
        $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>


@endsection