@extends('admin.includes.admin_design')

@section('site_title')
Add New Banner- {{ $themes->website_name}}
 @endsection

@section('content')


 <!--start content-->
 <main class="page-content">





<div class="row">
    <div class="col-12 col-lg-12">
        <div class="card shadow-sm border-0">
            <div class="card-body">
                <h5 class="mb-0">Add New Banner</h5>
                <hr>
                <div class="card shadow-none border">

                    @include('admin.includes._message')

                    <div class="card-body">
                        <form class="row g-3" method="post" action="{{route('banner.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="col-6">
                                <label for="banner_title" class="form-label">Banner Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" value="{{old('banner_title')}}" name="banner_title" id="banner_title">
                            </div>

                            <div class="col-4">
                                <label for="banner_subtitle" class="form-label">Banner  Sub Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" value="{{old('banner_subtitle')}}" name="banner_subtitle" id="banner_subtitle">
                            </div>

                            <div class="col-2">
                                <label for="priority" class="form-label">Priority <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" value="{{old('priority')}}" name="priority" id="priority">
                            </div>

                            <div class="col-12">
                                <label for="details" class="form-label">Banner  Details <span class="text-danger">*</span></label>
                                <textarea name="details" id="details" class="form-control" cols="30" rows="10"></textarea>
                            </div>

                            <div class="col-4">
                                <label for="link_1_title" class="form-label">Link 1 Title </label>
                                <input type="text" class="form-control" value="{{old('link_1_title')}}" name="link_1_title" id="link_1_title">
                            </div>
                            <div class="col-8">
                                <label for="link_1_URL" class="form-label">Link 1 URL</label>
                                <input type="url" class="form-control" value="{{old('link_1_URL')}}" name="link_1_URL" id="link_1_URL">
                            </div>

                            <div class="col-4">
                                <label for="link_2_title" class="form-label">Link 2 Title </label>
                                <input type="text" class="form-control" value="{{old('link_2_title')}}" name="link_2_title" id="link_2_title">
                            </div>
                            <div class="col-8">
                                <label for="link_2_URL" class="form-label">Link 2 URL</label>
                                <input type="url" class="form-control" value="{{old('link_2_URL')}}" name="link_2_URL" id="link_2_URL">
                            </div>


                            <div class="col-6">
                                        <label for="image" class="form-label">Banner Image</label>
                                        <input type="file" class="form-control" value="" name="image" id="image" accept="image/*" onchange="readURL(this)">
                                    </div>

                                    <div class="col-6">
                                    <input type="checkbox" checked name="status" id="status" value="1" >
                                    </div>
                                    <img id="one" src="" alt="" style="width: 300px !important;">


                                   
                            <div class="text-start">
                                <button type="submit" class="btn btn-success px-4">Save Information</button>
                            </div>

                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div><!--end row-->

</main>
<!--end page main-->
@endsection

@section('js')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>
  $(function() {
    $('#status').bootstrapToggle({
      on: 'Mark as Active',
      off: 'Mark as Inactive'
    });
  })
</script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
    $(document).ready(function() {
  $('#details').summernote({
      height: 100
  });
});
</script>


    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection


