@extends('admin.includes.admin_design')


@section('site_title')
    Social Media Settings - {{ $themes->website_name }}
@endsection

@section('content')

    <main class="page-content">

        @include('admin.includes._message')
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <h5 class="mb-0">Social Settings
                        </h5>
                        <hr>
                        <form action="{{ route('socialUpdate', $social->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center">
                                            <p class="mb-0 text-right"><span class="text-danger">*</span> ARE REQUIRED FIELDS</p>
                                        </div>
                                        <hr/>
                                        <div class="row mb-3">
                                            <label for="facebook" class="col-sm-3 col-form-label">Facebook URL <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="url" class="form-control" id="facebook" name="facebook"  value="{{ $social->facebook }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="twitter" class="col-sm-3 col-form-label">Twitter URL <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="url" class="form-control" id="twitter" name="twitter"  value="{{ $social->twitter }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="instagram" class="col-sm-3 col-form-label">Instagram URL <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="url" class="form-control" id="instagram" name="instagram"  value="{{ $social->instagram }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="google_plus" class="col-sm-3 col-form-label">Google Plus URL <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="url" class="form-control" id="google_plus" name="google_plus"  value="{{ $social->google_plus }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="linkedin" class="col-sm-3 col-form-label">Linkedin URL <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="url" class="form-control" id="linkedin" name="linkedin"  value="{{ $social->linkedin }}">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <label class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-success px-5">Update Settings</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div><!--end row-->

    </main>

@endsection

@section('js')
    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(120);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#two").attr('src', e.target.result).width(60);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
