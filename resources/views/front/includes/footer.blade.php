 <!-- Footer Area-->
 <footer class="footer-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <div class="footer-widget">
                    <div class="logo">
                       
                        <img src="{{ asset('public/uploads/'.$themes->logo) }}" alt="logo">
                    </div>
                    <p>
 
                    {{ $themes->footer_info }}
                    </p>
                        
                    
                    @php
                        $social = \App\Models\Social::first();
                    @endphp


                    <ul class="footer-socials">
                        <li>
                          
                        @if(!empty($social->facebook))
                            <li>
                                <a href="{{ $social->facebook }}" target="_blank">
                                    <i class="lab la-facebook-f"></i>
                                </a>
                            </li>
                        @endif
                            @if(!empty($social->twitter))
                                <li>
                                    <a href="{{ $social->twitter }}" target="_blank">
                                        <i class="lab la-twitter"></i>
                                    </a>
                                </li>
                            @endif
                           
                            @if(!empty($social->instagram))
                                <li>
                                    <a href="{{ $social->instagram }}" target="_blank">
                                        <i class="lab la-instagram"></i>
                                    </a>
                                </li>
                            @endif
                            @if(!empty($social->linkedin))
                                <li>
                                    <a href="{{ $social->linkedin }}" target="_blank">
                                        <i class="lab la-linkedin"></i>
                                    </a>
                                </li>
                            @endif

                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="footer-widget">
                    <h3>Quick Links</h3>

                    <ul class="footer-text">
                        <li>
                            <a href="index.html">
                                <i class="las la-star"></i>
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="about.html">
                                <i class="las la-star"></i>
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="services.html">
                                <i class="las la-star"></i>
                                Our Services
                            </a>
                        </li>
                        <li>
                            <a href="case-study.html">
                                <i class="las la-star"></i>
                                Case Study
                            </a>
                        </li>
                        <li>
                            <a href="blog.html">
                                <i class="las la-star"></i>
                                Our Blog
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Clients Review
                            </a>
                        </li>
                        <li>
                            <a href="attorney.html">
                                <i class="las la-star"></i>
                                Our Attorneys
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="footer-widget pl-50">
                    <h3>Services</h3>

                    <ul class="footer-text">
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Civil Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Family Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Cyber Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Education Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Business Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Investment Law
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="las la-star"></i>
                                Criminal Law
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="footer-widget">
                    <h3>Contact Info</h3>

                    <ul class="info-list">
                        <li>
                            <i class="las la-phone"></i>
                            
                            <a href="tel:{{ $themes->phone }}">{{ $themes->phone }}</a>
                        </li>
                        <li>
                            <i class="las la-envelope"></i>
                            
                            <a href="mailto:{{ $themes->email }}">{{ $themes->email }}</a>
                        </li>
                        <li>
                            <i class="las la-map-marker-alt"></i>
                           
                            {{ $themes->address }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

        <!-- End Footer Area -->

        <!-- Footer bottom Area -->
        <div class="footer-bottom">
            <div class="container">
                <p>Copyright @ <?php echo date('Y'); ?> Atorn. All rights reserved <a href="https://hibootstrap.com/" target="_blank">HiBootstrap</a></p>
            </div>
        </div>
        <!-- End Footer bottom Area -->

        <!-- Go Top -->
        <div class="go-top">
            <i class="las la-hand-point-up"></i>
        </div>
        <!-- End Go Top -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{asset('public/frontend/assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('public/frontend/assets/js/popper.min.js')}}"></script>
        <script src="{{asset('public/frontend/assets/js/bootstrap.min.js')}}"></script>
        <!-- Meanmenu JS -->
        <script src="{{asset('public/frontend/assets/js/meanmenu.min.js')}}"></script>
        <!-- Magnific JS -->
        <script src="{{asset('public/frontend/assets/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Owl Carousel JS -->
        <script src="{{asset('public/frontend/assets/js/owl.carousel.min.js')}}"></script>
        <!-- Odometer JS -->
        <script src="{{asset('public/frontend/assets/js/odometer.min.js')}}"></script>
        <!-- Appear JS -->
        <script src="{{asset('public/frontend/assets/js/jquery.appear.js')}}"></script>
        <!-- Form Validator JS -->
		<script src="{{asset('public/frontend/assets/js/form-validator.min.js')}}"></script>
		<!-- Contact JS -->
		<script src="{{asset('public/frontend/assets/js/contact-form-script.js')}}"></script>
		<!-- Ajaxchimp JS -->
		<script src="{{asset('public/frontend/assets/js/jquery.ajaxchimp.min.js')}}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>

<script>
  $(function() {
    $('.status').bootstrapToggle({
      on: 'Mark As Active',
      off: 'Mark As Inactive'
    });
  })

  $(function() {
    $('.status_banner').bootstrapToggle({
      on: 'Active',
      off: 'Inactive'
    });
  })

</script>
        <!-- Custom JS -->
        <script src="{{asset('public/frontend/assets/js/custom.js')}}"></script>