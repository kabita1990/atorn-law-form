
        <!-- Heder Area -->
        <!-- Heder Area -->
<header class="header-area">
    <div class="top-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6">
                    <ul class="left-info">
                        <li>
                           
                            <a href="mailto:{{ $themes->email }}">
                                <i class="las la-envelope"></i>
                               
                                {{ $themes->email }}
                            </a>
                        </li>
                        <li>
                           
                            <a href="tel:{{ $themes->phone }}">
                                <i class="las la-phone"></i>
                                
                                {{ $themes->phone }}
                            </a>
                        </li>
                    </ul>
                </div>

                @php
                    $social = \App\Models\Social::first();
                    @endphp

                <div class="col-lg-6 col-sm-6">
                    <ul class="right-info">
                        @if(!empty($social->facebook))
                        <li>
                            <a href="#" target="_blank">
                            <a href="{{ $social->facebook }}" target="_blank">
                                <i class="lab la-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                        @endif
                            @if(!empty($social->twitter))
                            <li>
                            <a href="{{ $social->twitter }}" target="_blank">
                                <i class="lab la-twitter"></i>
                            </a>
                        </li>
                      
                        
                            @endif
                           
                            @if(!empty($social->instagram))
                                <li>
                                    <a href="{{ $social->instagram }}" target="_blank">
                                        <i class="lab la-instagram"></i>
                                    </a>
                                </li>
                            @endif
                            @if(!empty($social->linkedin))
                                <li>
                                    <a href="{{ $social->linkedin }}" target="_blank">
                                        <i class="lab la-linkedin"></i>
                                    </a>
                                </li>
                            @endif


                        <li class="heder-btn">
                            <a href="#">Get A Schedule</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


            <!-- Start Navbar Area -->
            <div class="navbar-area">
                <div class="atorn-responsive-nav">
                    <div class="container">
                        <div class="atorn-responsive-menu">
                            <div class="logo">
                                <a href="{{route('index')}}">
                                    <img src="{{asset('public/uploads/'.$themes->logo)}}" alt="logo">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="atorn-nav">
                    <div class="container">
                        <nav class="navbar navbar-expand-md navbar-light">
                            <a class="navbar-brand" href="{{route('index')}}">
                                <img src="{{asset('public/uploads/'.$themes->logo)}}" alt="logo">
                            </a>

                            <div class="collapse navbar-collapse mean-menu">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link active">
                                            Home <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="index.html" class="nav-link active">Home Demo - 1</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="index-2.html" class="nav-link">Home Demo - 2</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="index-3.html" class="nav-link">Home Demo - 3</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="about.html" class="nav-link">
                                            About Us
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Pages <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="about.html" class="nav-link">About</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="team.html" class="nav-link">Team</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="testimonials.html" class="nav-link">Testimonials</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="gallery.html" class="nav-link">Gallery</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="faq.html" class="nav-link">FAQ</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    User <i class="las la-angle-right"></i>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="nav-item">
                                                        <a href="sign-in.html" class="nav-link">Sign In</a>
                                                    </li>
            
                                                    <li class="nav-item">
                                                        <a href="sign-up.html" class="nav-link">Sign Up</a>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li class="nav-item">
                                                <a href="privacy-policy.html" class="nav-link">Privacy Policy</a>
                                            </li>
            
                                            <li class="nav-item">
                                                <a href="terms-condition.html" class="nav-link">Terms & Condition</a>
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a href="coming-soon.html" class="nav-link">Coming Soon</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="error-404.html" class="nav-link">404 Error Page</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Services <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="services.html" class="nav-link">Services</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="service-details.html" class="nav-link">Services Details</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Case Study  <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="case-study.html" class="nav-link">Case Study</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="case-study-details.html" class="nav-link">Case Study Details</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Attorney <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="attorney.html" class="nav-link">Attorney</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="attorney-details.html" class="nav-link">Attorney Details</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            Blog <i class="las la-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a href="blog.html" class="nav-link">Blog</a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="blog-details.html" class="nav-link">Blog Details</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a href="{{ route('contactUs') }}" class="nav-link">Contact</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0)" class="nav-link search-box">
                                            <i class="las la-search"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- End Navbar Area -->
        </header>
        <!-- End Heder Area -->

        <!-- Search Overlay -->
        <div class="search-overlay">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="search-overlay-layer"></div>
                    <div class="search-overlay-layer"></div>
                    <div class="search-overlay-layer"></div>
                    
                    <div class="search-overlay-close">
                        <span class="search-overlay-close-line"></span>
                        <span class="search-overlay-close-line"></span>
                    </div>

                    <div class="search-overlay-form">
                        <form>
                            <input type="text" class="input-search" placeholder="Search here...">
                            <button type="submit"><i class='las la-search'></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Search Overlay -->