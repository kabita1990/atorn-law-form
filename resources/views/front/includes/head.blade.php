<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/bootstrap.min.css')}}">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/animate.min.css')}}">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/meanmenu.min.css')}}">
        <!-- Line Awesome CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/line-awesome.min.css')}}">
        <!-- Magnific CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/magnific-popup.css')}}">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/owl.carousel.min.css')}}">
        <!-- Owl Theme CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/owl.theme.default.min.css')}}">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/odometer.css')}}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

        <!-- Stylesheet CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/style.css')}}">
        <!-- Stylesheet Responsive CSS -->
        <link rel="stylesheet" href="{{asset('public/frontend/assets/css/responsive.css')}}">

        <style>
        #editorText span{
            color: white !important;
            margin-bottom: 10px !important;
            line-height: 1.8 !important;
            font-family: "Merriweather", serif !important;
            font-size: 16px !important;
        }
    </style>

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{asset('public/uploads/'.$themes->favicon)}}">
        <!-- Title -->
        <title>@yield('site_title')</title>
    </head>
