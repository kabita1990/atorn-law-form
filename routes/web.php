<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/     
     // Index page or front page
    Route::get('/',[\App\Http\Controllers\Front\IndexPageController::class, 'index'])->name('index');

    //Contact us Page
    Route::get('/contact-us', [\App\Http\Controllers\Front\FrontEndController::class, 'contactUs'])->name('contactUs');

    //
    Route::post('/contact/message', [\App\Http\Controllers\Front\ContactMessageController::class, 'contactMessage'])->name('contactMessage');





Route::prefix('/admin')->group(function (){
    // Admin Login
    Route::get('/login', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminLogin'])->name('adminLogin');
    Route::post('/login', [\App\Http\Controllers\Admin\AdminLoginController::class, 'loginAdmin'])->name('loginAdmin');

    Route::group(['middleware' => 'admin'], function(){
        // Admin Dashboard
        Route::get('/dashboard', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminDashboard'])->name('adminDashboard');
        // Admin Profile
        Route::get('/profile', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfile'])->name('adminProfile');
        // Admin Profile Update
        Route::post('/profile/update/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfileUpdate'])->name('adminProfileUpdate');
        // Delete Image
        Route::get('/delete-image/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'deleteImage'])->name('deleteImage');
        // Change Password
        Route::get('/change-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'changePassword'])->name('changePassword');
        // Check Current Password
        Route::post('/check-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'chkUserPassword'])->name('chkUserPassword'); 

        //Theme Setting
         Route:: get('/theme',[\App\Http\controllers\Admin\ThemeController::class,'theme'])->name('theme');
         Route:: post('/theme/{id}',[\App\Http\controllers\Admin\ThemeController::class,'themeUpdate'])->name('themeUpdate');
         Route::get('/social', [\App\Http\Controllers\Admin\ThemeController::class, 'social'])->name('social');
         Route::post('/social/update/{id}', [\App\Http\Controllers\Admin\ThemeController::class, 'socialUpdate'])->name('socialUpdate');
 

         //Banner Route
         Route::get('/banners', [\App\Http\controllers\Admin\BannerController::class,'index'])->name('banner.index');
         Route::get('/banner/add', [\App\Http\controllers\Admin\BannerController::class,'add'])->name('banner.add');
         Route::post('/banner/store', [\App\Http\controllers\Admin\BannerController::class,'store'])->name('banner.store');
         Route::get('/table/banner',[\App\Http\controllers\Admin\BannerController::class,'dataTable'])->name('table.banner');
         Route::get('/changeStatus',[\App\Http\controllers\Admin\BannerController::class,'changeStatus'])->name('changeStatus');
         Route::get('/banner/show/{id}',[\App\Http\controllers\Admin\BannerController::class,'show'])->name('banner.show');
         Route::get('/banner/edit/{id}', [\App\Http\controllers\Admin\BannerController::class,'edit'])->name('banner.edit');
         Route::post('/banner/update/{id}', [\App\Http\controllers\Admin\BannerController::class,'update'])->name('banner.update');
         Route::get('/delete-banner/{id}', [\App\Http\controllers\Admin\BannerController::class,'delete'])->name('banner.delete');

          // Page Routes
        Route::get('/pages', [\App\Http\Controllers\Admin\PageController::class, 'index'])->name('page.index');
        Route::get('/page/add', [\App\Http\Controllers\Admin\PageController::class, 'add'])->name('page.add');
        Route::post('/page/store', [\App\Http\Controllers\Admin\PageController::class, 'store'])->name('page.store');
        Route::get('/table/page', [\App\Http\Controllers\Admin\PageController::class, 'dataTable'])->name('table.page');
        Route::get('/page/edit/{id}', [\App\Http\Controllers\Admin\PageController::class, 'edit'])->name('page.edit');
        Route::post('/page/update/{id}', [\App\Http\Controllers\Admin\PageController::class, 'update'])->name('page.update');
        Route::get('/delete-page/{id}', [\App\Http\Controllers\Admin\PageController::class, 'delete'])->name('page.delete');


    });

    Route::get('logout', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminLogout'])->name('adminLogout');


    //Reset Password

    Route::match(['get','post'],'/reset/password',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminResetPassword'])->name('adminResetPassword');
});

