<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use App\Models\Social;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Admin::insert([
            'name' => 'Kabita bhatta',
            'email' => 'kabitabhatta1998@gmail.com',
            'password' => bcrypt('password'),
        ]);


         
        Theme::insert([
            'website_name'=>"Tech coderz",
            'website_tagline'=>"Inspire the Next",
              'favicon'=> "",
          ]);

           
        Social::insert([
            'facebook'=>"",
          ]);
    }
}
