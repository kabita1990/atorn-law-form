<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('banner_title');
            $table->string('banner_subtitle');
            $table->string('image');
            $table->LongText('details');
            $table->enum('status', ['active', 'inactive']);
            $table->integer('priority')->default(0);
            $table->string('link_1_title')->nullable();
            $table->string('link_2_title')->nullable();
            $table->string('link_1_URL')->nullable();
            $table->string('link_2_URL')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
